﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public void StartGame()
    {
        OnStartGame();
    }


    protected virtual void OnStartGame() { }
}
