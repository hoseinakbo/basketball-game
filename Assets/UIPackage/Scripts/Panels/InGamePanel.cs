﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGamePanel : GamePanel {

    public PlayerUIDetails playerDetails;/*
    public Transform potionParent;
    public GameObject potionButtonPrefab;
    public PotionButton[] potionButtons;

    PotionButton[] activePotionButtons;*/

    protected override void OnActivate()
    {
    }


    public void SetPlayerDetails(string playerName, int playerLevel)
    {
        playerDetails.nameText.text = playerName;
        playerDetails.levelText.text = playerLevel.ToString();
    }

    public void SetScore(int score)
    {
        playerDetails.scoreText.text = score.ToString();
    }

    public void SetTimer(int time)
    {
        playerDetails.timerText.text = time.ToString();
    }
    /*
    public void SetPotions(Potion[] potions)
    {
        potionButtons = new PotionButton[potions.Length];
        for (int i = 0; i < potions.Length; i++)
        {
            potionButtons[i] = Instantiate(potionButtonPrefab, potionParent).GetComponent<PotionButton>();
            potionButtons[i].transform.localPosition = new Vector3(-10, 10+130*i, 0);
            potionButtons[i].transform.localScale = new Vector3(1, 1, 1);
            potionButtons[i].nameText.text = potions[i].name;
            potionButtons[i].numberText.text = potions[i].value.ToString();
            potionButtons[i].button.onClick.AddListener(PotionButtonOnClick);
        }
    }*/
    /*
    public void SetPotions(Potion[] potions)
    {
        for (int i = 0; i < potionButtons.Length; i++)
            potionButtons[i].SetToDeactive();
        activePotionButtons = new PotionButton[potions.Length];
        for (int i = 0; i < potions.Length; i++)
        {
            PotionButton button = null;
            switch (potions[i].name)
            {
                case "speed":
                    button = potionButtons[(int)PotionType.Speed];
                    break;
                case "accuracy":
                    button = potionButtons[(int)PotionType.Accuracy];
                    break;
                case "stabletarget":
                    button = potionButtons[(int)PotionType.StableTarget];
                    break;
                case "scorex3":
                    button = potionButtons[(int)PotionType.ScoreX3];
                    break;
                default:
                    break;
            }
            if (button != null)
            {
                activePotionButtons[i] = button;
                //activePotionButtons[i].transform.localPosition = new Vector3(-10, 10 + 130 * i, 0);
                //activePotionButtons[i].transform.localScale = new Vector3(1, 1, 1);
                //activePotionButtons[i].nameText.text = potions[i].name;
                activePotionButtons[i].number = potions[i].value;
                activePotionButtons[i].numberText.text = potions[i].value.ToString();
                //activePotionButtons[i].potionType = (PotionType)i;
                //activePotionButtons[i].button.onClick.AddListener(PotionButtonOnClick);
                activePotionButtons[i].SetToActive();
            }
        }
    }

    public void ResetPotionButtons()
    {
        if(activePotionButtons != null)
        for (int i = 0; i < activePotionButtons.Length; i++)
        {
            activePotionButtons[i].ResetButton();
        }
    }*/
}
