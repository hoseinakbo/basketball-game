﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePanel : MonoBehaviour {

    public CanvasGroup canvasGroup;

    public void Activate()
    {
        /*if (canvasGroup.alpha == 1)
            return;*/
        OnActivate();
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;
    }

    public void Deactivate()
    {
        /*if (canvasGroup.alpha == 0)
            return;*/
        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;
        OnDeactivate();
    }


    protected virtual void OnActivate() { }
    protected virtual void OnDeactivate() { }
}
