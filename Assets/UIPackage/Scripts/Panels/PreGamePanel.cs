﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreGamePanel : GamePanel
{
    public PlayerUIDetails playerDetails;
    public PlayerUIDetails opponentDetails;
    public GameObject playGameButton;

    protected override void OnActivate()
    {
        playGameButton.SetActive(false);
        SetPlayerDetails("Hosein", 12);
    }

    public void SetPlayerDetails(string playerName, int playerLevel)
    {
        playerDetails.nameText.text = playerName;
        playerDetails.levelText.text = playerLevel.ToString();
    }

    public void SetOpponentDetails(string oppName, int oppLevel)
    {
        opponentDetails.nameText.text = oppName;
        opponentDetails.levelText.text = oppLevel.ToString();
    }

    public void ShowPlayGameButton()
    {
        playGameButton.SetActive(true);
    }
}
