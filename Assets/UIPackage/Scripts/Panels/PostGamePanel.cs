﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PostGamePanel : GamePanel {
    
    public PlayerUIDetails playerDetails;
    public PlayerUIDetails opponentDetails;
    public GameObject nextGameButton;
    public GameObject detailsObject;
    public GameObject loadingScoresObject;
    public Text nextGameButtonText;

    protected override void OnActivate()
    {
        if (!NetworkManager.Instance.offlineMode)
        {
            nextGameButton.SetActive(false);
            detailsObject.SetActive(false);
            loadingScoresObject.SetActive(true);
        }
        else
        {
            nextGameButtonText.text = "Back to Farm";
            nextGameButton.SetActive(true);
            detailsObject.SetActive(true);
            loadingScoresObject.SetActive(false);
        }
    }

    public void SetDetails(string playerName, int playerLevel, string oppName, int oppLevel)
    {
        playerDetails.nameText.text = playerName;
        playerDetails.levelText.text = playerLevel.ToString();
        opponentDetails.nameText.text = oppName;
        opponentDetails.levelText.text = oppLevel.ToString();
    }

    public void ShowGameResult(int[] playerResults, int[] oppResults, string nextGame)
    {
        int playerScore = playerResults[playerResults.Length - 1];
        int opponentScore = oppResults[oppResults.Length - 1];

        playerDetails.scoreText.text = playerScore.ToString();
        opponentDetails.scoreText.text = opponentScore.ToString();

        if (playerScore > opponentScore)
        {
            playerDetails.winnerCrown.SetActive(true);
            opponentDetails.winnerCrown.SetActive(false);
            playerDetails.prizeText.text = "$1000";
            opponentDetails.prizeText.text = "$0";
        }
        else if (playerScore < opponentScore)
        {
            playerDetails.winnerCrown.SetActive(false);
            opponentDetails.winnerCrown.SetActive(true);
            playerDetails.prizeText.text = "$0";
            opponentDetails.prizeText.text = "$1000";
        }
        else
        {
            playerDetails.winnerCrown.SetActive(false);
            opponentDetails.winnerCrown.SetActive(false);
            playerDetails.prizeText.text = "$500";
            opponentDetails.prizeText.text = "$500";
        }

        if (nextGame == null)
            nextGameButtonText.text = "Back to Farm";
        else
            nextGameButtonText.text = "Next Game";

        loadingScoresObject.SetActive(false);
        detailsObject.SetActive(true);
        nextGameButton.SetActive(true);
    }
}
