﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PotionButton : MonoBehaviour {

    public Button button;
    public Text nameText;
    public Text numberText;
    [HideInInspector]
    public int number;
    
    public void PotionButtonOnClick()
    {
        //if (number == 1)
            SetToDeactive();
        number -= 1;
        numberText.text = number.ToString();
    }

    public void SetToActive()
    {
        button.interactable = true;
    }

    public void SetToDeactive()
    {
        button.interactable = false;
    }
    public void ResetButton()
    {
        if (number > 0)
            SetToActive();
    }
}
