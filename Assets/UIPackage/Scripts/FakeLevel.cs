﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeLevel : MonoBehaviour {
    
    int score;
    int timer;

    public void Activate()
    {
        score = 0;
        gameObject.SetActive(true);
        UIPackage.Instance.SetScore(score);
        StartCoroutine("Timer");
    }
    
    public void EndLevelOnClick()
    {
        UIPackage.Instance.EndGame(score);
        gameObject.SetActive(false);
    }

    public void Add10ScoreOnClick()
    {
        score += 10;
        UIPackage.Instance.SetScore(score);
    }

    public void ResetTimerOnClick()
    {
        StopCoroutine("Timer");
        StartCoroutine("Timer");    
    }

    IEnumerator Timer()
    {
        timer = 60;
        UIPackage.Instance.SetTimer(timer);
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (timer - 1 == 0)
            {
                EndLevelOnClick();
                StopCoroutine("Timer");
            }
            timer -= 1;
            UIPackage.Instance.SetTimer(timer);
        }
    }



}
