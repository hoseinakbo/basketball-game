﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPackage : MonoBehaviour
{
    [HideInInspector]
    public UIManager uiManager;

    public Minigame minigame;
    public GamePanel thisGamePanel;
    public GameManager thisGameManager;

    static UIPackage _instance;

    public static UIPackage Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<UIPackage>();

            return _instance;
        }
    }

    void Awake()
    {
        uiManager = GetComponentInChildren<UIManager>();
    }

    public void SetScore(int score)
    {
        uiManager.inGamePanel.SetScore(score);
    }

    public void SetTimer(int time)
    {
        uiManager.inGamePanel.SetTimer(time);
    }

    public void EndGame(int finalScore)
    {
        uiManager.EndGame();
    }

    public int[] GetLevels()
    {
        //Return levels specified by server
        int[] mamad = {Random.Range(0, 3), Random.Range(0, 3), Random.Range(0, 3) , Random.Range(0, 3) , Random.Range(0, 3) };
        return mamad;
    }
    
}
