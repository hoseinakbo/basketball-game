﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public PreGamePanel preGamePanel;
    public InGamePanel inGamePanel;
    public PostGamePanel postGamePanel;

    ReceiveUserJSON userJSON;
    ReceiveGameResultJSON gameResultJSON;

    #region Unity Functions

    void Start () {
        ReadyGame();

        NetworkManager.Instance.CommandFetchUser(OnFetchUser);
        ShowPanel(GameState.PreGame);
	}
	
	void Update () {
		
	}

    #endregion

    #region Button OnClick Functions

    public void BackButtonOnClick()
    {
        Debug.Log("Load Farm");
        SceneManager.LoadScene("MainMenu");
    }

    public void OfflineModeOnClick()
    {
        NetworkManager.Instance.offlineMode = true;
        userJSON = new ReceiveUserJSON();
        userJSON.name = "Dude";
        userJSON.level = 5;
        userJSON.potions = new Potion[7];
        userJSON.potions[0] = new Potion("speed", 8);
        userJSON.potions[1] = new Potion("accuracy", 8);
        userJSON.potions[2] = new Potion("stabletarget", 8);
        userJSON.potions[3] = new Potion("scorex3", 7);
        userJSON.potions[4] = new Potion("autodetect", 6);
        userJSON.potions[5] = new Potion("removeobstacle", 8);
        userJSON.potions[6] = new Potion("addammo", 7);
        userJSON.gameID = 1;
        PotionManager.instance.SetPotions(userJSON.potions);
        inGamePanel.SetPlayerDetails("Hosein", 12);
        UIPackage.Instance.thisGamePanel.Activate();
        ShowPanel(GameState.InGame);
        UIPackage.Instance.thisGameManager.StartGame();
    }

    public void PlayGameOnClick()
    {
        NetworkManager.Instance.offlineMode = false;
        inGamePanel.SetPlayerDetails("Hosein", 12);
        UIPackage.Instance.thisGamePanel.Activate();
        ShowPanel(GameState.InGame);
        UIPackage.Instance.thisGameManager.StartGame();
    }

    public void NextGameOnClick()
    {
        if (gameResultJSON != null)
        {
            if (gameResultJSON.nextGame == null)
                Debug.Log("Load Farm");
            else
                Debug.Log("Load Next Game");
        }
        SceneManager.LoadScene("MainMenu");
    }

    #endregion

    #region Functions For Outer Scripts

    void OnFetchUser(ReceiveJSON json)
    {
        userJSON = (ReceiveUserJSON)json;
        PotionManager.instance.SetPotions(userJSON.potions);
        preGamePanel.SetOpponentDetails(userJSON.name, userJSON.level);
        preGamePanel.ShowPlayGameButton();
    }

    void OnGameResult(ReceiveJSON json)
    {
        gameResultJSON = (ReceiveGameResultJSON)json;
        postGamePanel.ShowGameResult(gameResultJSON.previousResults, gameResultJSON.otherUserResults, gameResultJSON.nextGame);
    }

    public void EndGame()
    {
        if (!NetworkManager.Instance.offlineMode)
            NetworkManager.Instance.CommandGameResult(OnGameResult);
        postGamePanel.SetDetails("Hosein", 12, userJSON.name, userJSON.level);
        UIPackage.Instance.thisGamePanel.Deactivate();
        ShowPanel(GameState.PostGame);
    }

    /*public string GetPlayerName()
    {
        return "Hosein";
    } */

    #endregion

    #region Helper Functions

    IEnumerator ShortWait(float time, Action method)
    {
        yield return new WaitForSeconds(time);
        method();
    } 

    void ShowPanel(GameState state)
    {
        switch (state)
        {
            case GameState.PreGame:
                preGamePanel.Activate();
                inGamePanel.Deactivate();
                postGamePanel.Deactivate();
                break;
            case GameState.InGame:
                preGamePanel.Deactivate();
                inGamePanel.Activate();
                postGamePanel.Deactivate();
                break;
            case GameState.PostGame:
                preGamePanel.Deactivate();
                inGamePanel.Deactivate();
                postGamePanel.Activate();
                break;
        }
    }
    
    void ReadyGame()
    {
        preGamePanel.gameObject.SetActive(true);
        preGamePanel.GetComponent<CanvasGroup>().alpha = 1;
        preGamePanel.GetComponent<CanvasGroup>().blocksRaycasts = true;
        inGamePanel.gameObject.SetActive(true);
        inGamePanel.GetComponent<CanvasGroup>().alpha = 0;
        inGamePanel.GetComponent<CanvasGroup>().blocksRaycasts = false;
        postGamePanel.gameObject.SetActive(true);
        postGamePanel.GetComponent<CanvasGroup>().alpha = 0;
		postGamePanel.GetComponent<CanvasGroup>().blocksRaycasts = false;
        UIPackage.Instance.thisGamePanel.gameObject.SetActive(true);
        UIPackage.Instance.thisGamePanel.GetComponent<CanvasGroup>().alpha = 0;
        UIPackage.Instance.thisGamePanel.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    #endregion
}

public enum GameState
{
    PreGame,
    InGame,
    PostGame
}
