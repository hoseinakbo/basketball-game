﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIDetails : MonoBehaviour {

    public Text nameText;
    public Text levelText;
    public Image playerImage;
    public Text timerText;
    public Text scoreText;
    public Text prizeText;
    public GameObject winnerCrown;
}
