﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour {

    public static NetworkManager Instance;

    string serverURL = "http://habibtm.ir/server";

    string phone = "09203170740";
    string uniqueid = "12345";
    int gameID = -1;
    int result = 0;

    [HideInInspector]
    public bool offlineMode = false;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    
    void Start()
    {
        //CommandFetchUser();
        //CommandGameResult();
    }

    IEnumerator WaitForRequest(WWW www, RequestType reqType, Action<ReceiveJSON> onResultAction)
    {
        yield return www;

        switch (reqType)
        {
            case RequestType.FetchUser:
                if (www.error == null)
                {
                    Debug.Log("WWW Ok!: " + www.data);
                    OnFetchUser(www.data, onResultAction);
                }
                else {
                    Debug.Log("WWW Error: " + www.error);
                    //Debug.Log("Retrying...");
                    //CommandFetchUser(onResultAction);
                }
                break;
            case RequestType.GameResult:
                if (www.error == null)
                {
                    Debug.Log("WWW Ok!: " + www.data);
                    OnGameResult(www.data, onResultAction);
                }
                else {
                    Debug.Log("WWW Error: " + www.error);
                    //Debug.Log("Retrying...");
                    //CommandGameResult(onResultAction);
                }
                break;
        }
    }
    
    #region Commands

    public void CommandFetchUser(Action<ReceiveJSON> onResultAction)
    {
        string url = serverURL + "?mode=fetchUser&phone=" + phone + "&uniqueid=" + uniqueid;
        if (gameID != -1)
            url += "&game_id=" + gameID;
        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www, RequestType.FetchUser, onResultAction));
    }

    public void CommandGameResult(Action<ReceiveJSON> onResultAction)
    {
        string url = serverURL + "?mode=gameResult&phone=" + phone + "&uniqueid=" + uniqueid + "&game_id=" + gameID + "&result=" + result;
        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www, RequestType.GameResult, onResultAction));
    }

    #endregion


    #region Listening

    void OnFetchUser(string data, Action<ReceiveJSON> onResultAction)
    {
        /*Debug.Log("Opponent joined");
        string data = socketIOEvent.data.ToString();
        UserJSON userJSON = UserJSON.CreateFromJSON(data);
        opponent.playerName = userJSON.name;
        opponent.score = userJSON.score;
        opponent.OnStatsChange();*/

        ReceiveUserJSON userJSON = ReceiveUserJSON.CreateFromJSON(data);
        //Debug.Log(userJSON.potion);
        onResultAction(userJSON);
    }

    void OnGameResult(string data, Action<ReceiveJSON> onResultAction)
    {
        ReceiveGameResultJSON gameResultJSON = ReceiveGameResultJSON.CreateFromJSON(data);
        onResultAction(gameResultJSON);
    }

    #endregion

    /*#region JSONMessageClasses
    


    #endregion*/
}


public enum RequestType
{
    FetchUser,
    GameResult
}

public class ReceiveJSON
{

}

[Serializable]
public class ReceiveUserJSON : ReceiveJSON
{
    public string name;
    public int level;
    public Potion[] potions;
    public int gameID;
    public LevelProperties levelProperties;

    public static ReceiveUserJSON CreateFromJSON(string data)
    {
        JSONObject jsonObj = new JSONObject(data);
        ReceiveUserJSON userJSON = new ReceiveUserJSON();
        userJSON.name = jsonObj["name"].str;
        userJSON.level = (int)jsonObj["level"].f;
        JSONObject temp = jsonObj["potion"];
        userJSON.potions = new Potion[temp.Count];
        for (int i = 0; i < temp.Count; i++)
            userJSON.potions[i] = new Potion(temp[i]["name"].str, (int)temp[i]["value"].f);
        //Very very temporary:
        userJSON.potions[0].name = "speed";
        if(userJSON.potions.Length > 1)
            userJSON.potions[1].name = "accuracy";
        if (userJSON.potions.Length > 2)
            userJSON.potions[2].name = "stabletarget";
        if (userJSON.potions.Length > 3)
            userJSON.potions[3].name = "scorex3";
        //end temp
        userJSON.gameID = (int)jsonObj["game_id"].f;
        temp = jsonObj["game"];
        userJSON.levelProperties = new LevelProperties(temp.GetField("name").str);
        return userJSON;
    }
}

[Serializable]
public class ReceiveGameResultJSON : ReceiveJSON
{
    public int result;
    public int[] previousResults;
    public int[] otherUserResults;
    public string nextGame;

    public static ReceiveGameResultJSON CreateFromJSON(string data)
    {
        JSONObject jsonObj = new JSONObject(data);
        ReceiveGameResultJSON gameResultJSON = new ReceiveGameResultJSON();

        if(jsonObj["result"] != null)
            gameResultJSON.result = (int)jsonObj["result"].f;

        int count = jsonObj["previousResult"].Count;
        gameResultJSON.previousResults = new int[count];
        gameResultJSON.otherUserResults = new int[count];
        for (int i = 0; i < count; i++)
        {
            gameResultJSON.previousResults[i] = (int)jsonObj["previousResult"][i].f;
            gameResultJSON.otherUserResults[i] = (int)jsonObj["otherUserResult"][i].f;
        }

        if (jsonObj["nextGame"] != null)
            gameResultJSON.nextGame = jsonObj["nextGame"].str;

        return gameResultJSON;
    }
}

public class Potion
{
    public string name;
    public int value;

    public Potion(string name, int value)
    {
        this.name = name;
        this.value = value;
    }
}

public class LevelProperties
{
    public string gameName;

    public LevelProperties(string gameName)
    {
        this.gameName = gameName;
    }
}
