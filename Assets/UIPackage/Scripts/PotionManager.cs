﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionManager : MonoBehaviour
{

    static PotionManager _instance;

    public static PotionManager instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<PotionManager>();

            return _instance;
        }
    }
    
    public GameObject potionButtonPrefab;
    PotionButton[] potionButtons;

    PotionButton[] activePotionButtons;

    /*
    public void SetPotions(Potion[] potions)
    {
        potionButtons = new PotionButton[potions.Length];
        for (int i = 0; i < potions.Length; i++)
        {
            potionButtons[i] = Instantiate(potionButtonPrefab, potionParent).GetComponent<PotionButton>();
            potionButtons[i].transform.localPosition = new Vector3(-10, 10+130*i, 0);
            potionButtons[i].transform.localScale = new Vector3(1, 1, 1);
            potionButtons[i].nameText.text = potions[i].name;
            potionButtons[i].numberText.text = potions[i].value.ToString();
            potionButtons[i].button.onClick.AddListener(PotionButtonOnClick);
        }
    }*/
    void Start()
    {
        potionButtons = GetComponentsInChildren<PotionButton>();
    }

    public void SetPotions(Potion[] potions)
    {
        for (int i = 0; i < potionButtons.Length; i++)
            potionButtons[i].SetToDeactive();
        switch (UIPackage.Instance.minigame)
        {
            case Minigame.Archery:
                activePotionButtons = new PotionButton[5];
                break;
            case Minigame.Basketball:
                activePotionButtons = new PotionButton[5];
                break;
            case Minigame.Sniper:
                activePotionButtons = new PotionButton[4];
                break;
            default:
                break;
        }
        int activeI = 0;
        for (int i = 0; i < potions.Length; i++)
        {
            PotionButton button = null;
            switch (UIPackage.Instance.minigame)
            {
                case Minigame.Archery:
                    switch (potions[i].name)
                    {
                        case "speed":
                            button = potionButtons[0];
                            break;
                        case "accuracy":
                            button = potionButtons[1];
                            break;
                        case "stabletarget":
                            button = potionButtons[2];
                            break;
                        case "scorex3":
                            button = potionButtons[3];
                            break;
                        case "addammo":
                            button = potionButtons[4];
                            break;
                        default:
                            break;
                    }
                    break;
                case Minigame.Basketball:
                    switch (potions[i].name)
                    {
                        case "speed":
                            button = potionButtons[0];
                            break;
                        case "stabletarget":
                            button = potionButtons[1];
                            break;
                        case "scorex3":
                            button = potionButtons[2];
                            break;
                        case "removeobstacle":
                            button = potionButtons[3];
                            break;
                        case "addammo":
                            button = potionButtons[4];
                            break;
                        default:
                            break;
                    }
                    break;
                case Minigame.Sniper:
                    switch (potions[i].name)
                    {
                        case "speed":
                            button = potionButtons[0];
                            break;
                        case "autodetect":
                            button = potionButtons[1];
                            break;
                        case "scorex3":
                            button = potionButtons[2];
                            break;
                        case "addammo":
                            button = potionButtons[3];
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            if (button != null)
            {
                activePotionButtons[activeI] = button;
                //activePotionButtons[activeI].transform.localPosition = new Vector3(-10, 10 + 130 * i, 0);
                //activePotionButtons[activeI].transform.localScale = new Vector3(1, 1, 1);
                //activePotionButtons[activeI].nameText.text = potions[i].name;
                activePotionButtons[activeI].number = potions[i].value;
                activePotionButtons[activeI].numberText.text = potions[i].value.ToString();
                //activePotionButtons[activeI].potionType = (PotionType)i;
                //activePotionButtons[activeI].button.onClick.AddListener(PotionButtonOnClick);
                activePotionButtons[activeI].SetToActive();
                activeI++;
            }
        }
    }

    public void ResetPotionButtons()
    {
        if (activePotionButtons != null)
            for (int i = 0; i < activePotionButtons.Length; i++)
            {
                activePotionButtons[i].ResetButton();
            }
    }
}
/*
public enum PotionType
{
    Speed,
    Accuracy,
    StableTarget,
    ScoreX3,
    Type5
}*/

public enum Minigame
{
    Archery,
    Basketball,
    Sniper
}