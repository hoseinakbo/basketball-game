﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketballManager : MonoBehaviour
{

    static BasketballManager _instance;

    public static BasketballManager instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<BasketballManager>();

            return _instance;
        }
    }
    
    //public float basketballDisapearTime = 4;

    public BasketballLevelManager levelManager;
    public DesignBasketball designBasketball;
    public ScoreManagerBasketball scoreManager;
    public PotionsManagerBasketball potionsManager;
    public BasketballFlickManager flickManager;

    [HideInInspector]
    public bool shouldLoadNextLevel = false;
    [HideInInspector]
    public BasketballPanel basketballPanel;

    BasketballPhysics currentBasketball;
    List<BasketballPhysics> activeBasketballs;
    Vector3 startingPos;
    
    float gameTimer;
    float reloadTimer;
    float reloadSpeed;

    bool isTimer;

    void Start()
    {
        basketballPanel = (BasketballPanel)UIPackage.Instance.thisGamePanel;
    }
    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.A))
            Scored();*/
        if (isTimer && GameManagerBasketball.instance.IsPlaying)
        {
            gameTimer -= Time.deltaTime;
            UIPackage.Instance.SetTimer((int)gameTimer);
            if ((int)gameTimer == 0)
            {
                StopAllCoroutines();
                GameManagerBasketball.instance.EndGame();
                scoreManager.PauseScoreTimer();
                UIPackage.Instance.EndGame(scoreManager.GetTotalScore());
            }
        }
    }

    public void StartGame() {
        levelManager.SetLevels();
        levelManager.LoadNextLevel();
        reloadTimer = designBasketball.reloadTimeNormal;
        gameTimer = designBasketball.gameTotalTime;
        flickManager.StartGame();
        StartTimer();
        reloadSpeed = 1;
    }

    public void StartLevel(Vector3 startingPos)
    {
        DestroyAllBasketballs();
        this.startingPos = startingPos;
        ResetPotions();
        GenerateBasketball();
        scoreManager.StartScoreTimer();
    }

    //Not in use (flick manager is in use)
    public void ThrowBasketball(Vector2 pos1, Vector2 pos2, float time)
    {
        if (currentBasketball == null)
            return;
        Vector2 direction = (pos2 - pos1);
        currentBasketball.ThrowBall(direction / time);
        currentBasketball = null;
        StartCoroutine("SetNextBasketball");
    }
    public BasketballPhysics GetBall()
    {
        if (currentBasketball == null)
            return null;
        BasketballPhysics temp = currentBasketball;
        currentBasketball = null;
        StartCoroutine("SetNextBasketball");
        return temp;
    }

    IEnumerator SetNextBasketball()
    {
        reloadTimer = designBasketball.reloadTimeNormal;
        //yield return new WaitForSeconds(designBasketball.reloadTimeNormal);
        while (reloadTimer > 0)
        {
            reloadTimer -= Time.deltaTime * reloadSpeed;
            //Debug.Log(reloadTimer);
            basketballPanel.SetReloadTime(reloadTimer, designBasketball.reloadTimeNormal);
            yield return null;
        }
        basketballPanel.HideReloadBar();
        //reloadTimer = designBasketball.reloadTimeNormal;
        /*if(shouldLoadNextLevel)
        {
            shouldLoadNextLevel = false;
            levelManager.LoadNextLevel();
        }
        else*/
            GenerateBasketball();
    }

    void GenerateBasketball()
    {
        currentBasketball = BasketballPool.instance.CreateBasketball().GetComponent<BasketballPhysics>();
        activeBasketballs.Add(currentBasketball);
        currentBasketball.transform.position = startingPos;
        currentBasketball.OnActivating();
    }

    public void DestroyBasketball(BasketballPhysics basketball)
    {
        basketball.isActive = false;
        activeBasketballs.Remove(basketball);
        BasketballPool.instance.DestroyBasketball(basketball.gameObject);
    }

    void DestroyAllBasketballs()
    {
        StopAllCoroutines();
        if (activeBasketballs != null)
        {
            for (int i = 0; i < activeBasketballs.Count; i++)
            {
                activeBasketballs[i].isActive = false;
                BasketballPool.instance.DestroyBasketball(activeBasketballs[i].gameObject);
            }
        }
        activeBasketballs = new List<BasketballPhysics>();
    }

    void ResetPotions()
    {
        potionsManager.ResetPowerUp();
    }

    public void StartTimer()
    {
        isTimer = true;
    }
    public void PauseTimer()
    {
        isTimer = false;
    }

    public void ChangeReloadSpeed(float speed)
    {
        reloadSpeed = speed;
    }


    public void LevelTimeUp()
    {
        levelManager.LoadNextLevel();
    }
    public void Scored()
    {
        StopCoroutine("SetNextBasketball");
        scoreManager.AddScore();
        basketballPanel.ShowScoreEffect(scoreManager.GetCurrentScore());
        scoreManager.PauseScoreTimer();
        basketballPanel.HideReloadBar();
        shouldLoadNextLevel = true;
        StartCoroutine("LoadNextLevel");
    }
    IEnumerator LoadNextLevel()
    {
        yield return new WaitForSeconds(designBasketball.waitAfterScored);
        basketballPanel.HideScoreEffect();
        shouldLoadNextLevel = false;
        levelManager.LoadNextLevel();
    }
}
