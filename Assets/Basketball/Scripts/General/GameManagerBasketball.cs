﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerBasketball : GameManager
{
    bool isPlaying;

    static GameManagerBasketball _instance;
    public static GameManagerBasketball instance
    {
        get
        {
            if (!_instance)
                _instance = FindObjectOfType<GameManagerBasketball>();
            return _instance;
        }
    }

    protected override void OnStartGame()
    {
        isPlaying = true;
        BasketballManager.instance.StartGame();
    }
    public void EndGame()
    {
        Debug.Log("End Game");
        isPlaying = false;
    }

    public bool IsPlaying
    {
        get { return isPlaying; }
    }
}
