﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionsManagerBasketball : MonoBehaviour {

    public void SpeedOnClick()
    {
        BasketballManager.instance.ChangeReloadSpeed(BasketballManager.instance.designBasketball.reloadTimeNormal / BasketballManager.instance.designBasketball.reloadTimePowerUp);
    }

    public void StableTargetOnClick()
    {
        BasketballManager.instance.levelManager.activeLevel.netMovingPlatform.isMoving = false;
    }

    public void ScoreX3OnClick()
    {
        BasketballManager.instance.scoreManager.ChangeScoreMultiplier(BasketballManager.instance.designBasketball.scoreMultiplierPowerUp);
    }

    public void RemoveObstacleOnClick()
    {
        BasketballManager.instance.levelManager.activeLevel.DeactivateObstacle();
        if (BasketballManager.instance.levelManager.activeLevel.obstaclesToDeactivate > 0)
            BasketballManager.instance.basketballPanel.deactivateObstaclePotionButton.SetToActive();
    }

    public void AddBasketballsPotionOnClick()
    {
        Debug.Log("Todo");
    }

    public void ResetPowerUp()
    {
        if(BasketballManager.instance.levelManager.activeLevel.wasMoving)
            BasketballManager.instance.levelManager.activeLevel.netMovingPlatform.isMoving = true;
        BasketballManager.instance.scoreManager.ChangeScoreMultiplier(1);
        BasketballManager.instance.ChangeReloadSpeed(1);
        PotionManager.instance.ResetPotionButtons();
    }
}
