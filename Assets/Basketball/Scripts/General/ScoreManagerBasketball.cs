﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManagerBasketball : MonoBehaviour {

    int totalScore;
    int multiplier;
    float scoreTimer;
    int currentScore;

    // Use this for initialization
    void Start()
    {
        multiplier = 1;
    }
    
    public void AddScore()
    {
        currentScore = (int)(GetNumberOfStars() * BasketballManager.instance.designBasketball.starScore ) * multiplier;
        totalScore += currentScore;
        UIPackage.Instance.SetScore(totalScore);
    }

    public void ChangeScoreMultiplier(int mult)
    {
        multiplier = mult;
    }

    public void StartScoreTimer()
    {
        StopCoroutine("SetNextLevel");
        StartCoroutine("SetNextLevel");
    }

    public void PauseScoreTimer()
    {
        StopCoroutine("SetNextLevel");
    }

    IEnumerator SetNextLevel()
    {
        float levelTime = BasketballManager.instance.designBasketball.levelTime;
        BasketballPanel panel = BasketballManager.instance.basketballPanel;
        scoreTimer = levelTime;
        while (scoreTimer > 0)
        {
            scoreTimer -= Time.deltaTime;
            //Debug.Log(scoreTimer);
            panel.SetScoreBar(scoreTimer, levelTime);
            yield return null;
        }
        //scoreTimer = levelTime;
        BasketballManager.instance.basketballPanel.HideReloadBar();
        BasketballManager.instance.levelManager.LoadNextLevel();
    }

    int GetNumberOfStars()
    {
        if (BasketballManager.instance.designBasketball.levelTime - scoreTimer < BasketballManager.instance.designBasketball.threeStarTime)
            return 3;
        else if (BasketballManager.instance.designBasketball.levelTime - scoreTimer < BasketballManager.instance.designBasketball.twoStarTime)
            return 2;
        else
            return 1;
    }

    public int GetTotalScore()
    {
        return totalScore;
    }
    public int GetCurrentScore()
    {
        int temp = currentScore;
        currentScore = 0;
        return temp;
    }
}
