﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesignBasketball : MonoBehaviour {

    public float gameTotalTime = 60;
    public int starScore = 100;

    [Header("Normal Values")]
    public float reloadTimeNormal = 2;
    public float waitAfterScored = 2;
    public float levelTime = 25;
    public float threeStarTime = 10;
    public float twoStarTime = 15;

    [Header("PowerUp Values")]
    public float reloadTimePowerUp = 1;
    public int scoreMultiplierPowerUp = 3;

    [Header("UI Values")]
    public float scoreEffectTime = 2;
}
