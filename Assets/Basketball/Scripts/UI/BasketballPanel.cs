﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BasketballPanel : GamePanel {
    
    public Image reloadTimeImage;
    public Image scoreBarImage;
    public Text scoreEffectText;

    public PotionButton deactivateObstaclePotionButton;
    public PotionButton stopBasketPotionButton;

    public Image[] stars;
    float starTopY = 310;
    float starBottomY = -310;

    protected override void OnActivate()
    {
        HideReloadBar();
        scoreEffectText.gameObject.SetActive(false);
        SetStars();
    }
    
    public void SetReloadTime(float time, float totalTime)
    {
        reloadTimeImage.gameObject.SetActive(true);
        if (time < 0)
            time = 0;
        //reloadTimeText.text = time.ToString();
        reloadTimeImage.fillAmount = (time / totalTime);
    }
    public void HideReloadBar()
    {
        reloadTimeImage.gameObject.SetActive(false);
    }
    public void ShowScoreEffect(int score)
    {
        if (score > 0)
        {
            scoreEffectText.text = "+" + score;
            scoreEffectText.gameObject.SetActive(true);
        }
    }

    public void HideScoreEffect()
    {
        scoreEffectText.gameObject.SetActive(false);
    }

    public void SetScoreBar(float time, float totalTime)
    {
        if (time < 0)
            time = 0;
        scoreBarImage.fillAmount = (time / totalTime);
    }
    public void SetStars()
    {
        float totalTime = BasketballManager.instance.designBasketball.levelTime;
        float height = starTopY - starBottomY;

        Vector3 pos = stars[0].rectTransform.anchoredPosition;
        pos.y = starTopY - (height * totalTime / totalTime);
        stars[0].rectTransform.anchoredPosition = pos;

        pos = stars[1].rectTransform.anchoredPosition;
        pos.y = starTopY - (height * BasketballManager.instance.designBasketball.twoStarTime / totalTime);
        stars[1].rectTransform.anchoredPosition = pos;

        pos = stars[2].rectTransform.anchoredPosition;
        pos.y = starTopY - (height * BasketballManager.instance.designBasketball.threeStarTime / totalTime);
        stars[2].rectTransform.anchoredPosition = pos;
    }
}
