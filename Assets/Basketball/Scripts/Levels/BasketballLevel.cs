﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketballLevel : MonoBehaviour {

    public Transform startPosition;

    [HideInInspector]
    public MovingPlatform netMovingPlatform;
    [HideInInspector]
    public bool wasMoving = false;
    [HideInInspector]
    public int obstaclesToDeactivate;

    List<GameObject> obstacles = new List<GameObject>();

    bool awakeDone = false;

    void Awake()
    {
        if (!awakeDone)
        {
            foreach (Transform child in transform)
            {
                if (child.CompareTag("BasketballObstacle"))
                    obstacles.Add(child.gameObject);
            }
            obstaclesToDeactivate = obstacles.Count;
            netMovingPlatform = GetComponentInChildren<MovingPlatform>();
            if (netMovingPlatform.isMoving)
                wasMoving = true;
            awakeDone = true;
        }
    }

    public void Activate()
    {
        if (!awakeDone)
            Awake();
        startPosition.gameObject.SetActive(false);
        for (int i = 0; i < obstacles.Count; i++)
            obstacles[i].gameObject.SetActive(true);
        obstaclesToDeactivate = obstacles.Count;
        
        if (obstaclesToDeactivate == 0)
            BasketballManager.instance.basketballPanel.deactivateObstaclePotionButton.SetToDeactive();
        if(!wasMoving)
            BasketballManager.instance.basketballPanel.stopBasketPotionButton.SetToDeactive();
        
        gameObject.SetActive(true);
    }

    public void DeactivateObstacle()
    {
        if(obstaclesToDeactivate > 0)
        {
            for (int i = 0; i < obstacles.Count; i++)
            {
                if (obstacles[i].activeSelf)
                {
                    obstacles[i].SetActive(false);
                    obstaclesToDeactivate--;
                    return;
                }
            }
        }
    }
}
