﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketballLevelManager : MonoBehaviour {
    
    List<BasketballLevel> levels = new List<BasketballLevel>();

    [HideInInspector]
    public BasketballLevel activeLevel;

    int[] levelsToPlay;
    int currentLevelIndex;

    void Awake()
    {
        foreach (Transform child in transform)
        {
            BasketballLevel level = child.GetComponent<BasketballLevel>();
            if (level != null)
            {
                child.gameObject.SetActive(false);
                levels.Add(level);
            }
        }
    }

    void Start()
    {
        currentLevelIndex = -1;
    }

    public void SetLevels()
    {
        levelsToPlay = UIPackage.Instance.GetLevels();
    }

    public void LoadNextLevel()
    {
        if(currentLevelIndex+1 < levelsToPlay.Length)
            currentLevelIndex++;
        LoadLevel(levelsToPlay[currentLevelIndex]);
    }

    void LoadLevel(int i)
    {
        Debug.Log("Loaded level " + i);
        if(activeLevel != null)
            activeLevel.gameObject.SetActive(false);
        activeLevel = levels[i];
        BasketballManager.instance.StartLevel(levels[i].startPosition.position);
        levels[i].Activate();
    }

}
