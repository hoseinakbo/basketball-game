﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    public bool isMoving = true;

    public float speed = 2;

    [HideInInspector]
    public Vector3 startPosition;
    public Vector3 endPosition;
    
    Vector3 currentEndPos;
    
	void Start () {
        startPosition = transform.position;
        currentEndPos = endPosition;
	}
	
	void Update () {
        if(isMoving)
            MovePlatform();
	}

    void MovePlatform()
    {
        if (transform.position == currentEndPos)
            OnReachedEdge();
        Vector3 nextPos = Vector3.MoveTowards(transform.position, currentEndPos, Time.deltaTime * speed);
        transform.position = nextPos;
    }
    
    void OnReachedEdge()
    {
        if (currentEndPos == endPosition)
        {
            currentEndPos = startPosition;
        }
        else
        {
            currentEndPos = endPosition;
        }
    }
}
