﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingPlatform : MonoBehaviour {

    public bool isRotating = true;

    public float speed = 40;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (isRotating)
            RotatePlatform();

    }

    void RotatePlatform()
    {
        transform.Rotate(0, 0, speed * Time.deltaTime);
    }
}
