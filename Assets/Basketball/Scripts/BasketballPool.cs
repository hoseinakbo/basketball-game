﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasketballPool : MonoBehaviour
{
    public GameObject basketball;
    public int numberOfBasketballs = 20;

    Queue<GameObject> basketballPool = new Queue<GameObject>();

    GameObject tempBasketball;

    static BasketballPool _instance;

    public static BasketballPool instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<BasketballPool>();

            return _instance;
        }
    }

    // Use this for initialization
    void Awake()
    {
        AddBasketballsToQueue();
    }

    void AddBasketballsToQueue()
    {
        for (int i = 0; i < numberOfBasketballs; i++)
        {
            tempBasketball = Instantiate(basketball);
            tempBasketball.transform.parent = transform;
            tempBasketball.transform.position = transform.position;
            tempBasketball.SetActive(false);
            basketballPool.Enqueue(tempBasketball);
        }
    }

    public GameObject CreateBasketball()
    {
        GameObject currentBasketball = basketballPool.Dequeue();
        currentBasketball.GetComponent<Rigidbody2D>().simulated = false;
        Color color = currentBasketball.GetComponent<SpriteRenderer>().color;
        color.a = 255;
        currentBasketball.GetComponent<SpriteRenderer>().color = color;
        currentBasketball.SetActive(true);

        CheckNumberOfBasketBalls();

        return currentBasketball;
    }

    public void DestroyBasketball(GameObject currentBasketball)
    {
        currentBasketball.SetActive(false);
        currentBasketball.transform.position = transform.position;
        basketballPool.Enqueue(currentBasketball);
    }

    void CheckNumberOfBasketBalls()
    {
        if (basketballPool.Count <= 1)
            AddBasketballsToQueue();
    }
}
