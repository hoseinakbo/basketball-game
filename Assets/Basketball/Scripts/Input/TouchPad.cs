﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TouchPad : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public delegate void touchDelegate(Vector2 position, float time, TouchEvent touchEvent);
    public event touchDelegate OnTouch;



    public void OnBeginDrag(PointerEventData data)
    {
        if (OnTouch != null)
            OnTouch(data.position, Time.time, TouchEvent.Touched);
    }

    public void OnDrag(PointerEventData data)
    {
        if (OnTouch != null)
            OnTouch(data.position, Time.time, TouchEvent.Moved);
    }

    public void OnEndDrag(PointerEventData data)
    {
        if (OnTouch != null)
            OnTouch(data.position, Time.time, TouchEvent.Detouched);
    }
}

public enum TouchEvent
{
    Touched,
    Moved,
    Detouched
}