﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BasketballFlickManager : MonoBehaviour
{

    public Text forceText;

    public Vector2 touchStart;
    public Vector2 touchEnd;
    public int flickTime = 5;
    public int flickLength = 0;
    public float ballVelocity = 0.0f;
    public float ballSpeed = 0;
    public Vector3 worldAngle;

    private bool GetVelocity = false;
    public float comfortZone = 0.0f;
    public bool couldBeSwipe;
    public float startCountdownLength = 0.0f;
    public bool startTheTimer = false;
    static bool globalGameStart = false;
    static bool shootEnable = false;
    private float startGameTimer = 0.0f;
    
    float flickStartTime;
    float flickEndTime;
    /*
    void Start()
    {
        startTheTimer = true;
        Time.timeScale = 1;
        if (Application.isEditor)
        {
            Time.fixedDeltaTime = 0.01f;
        }
    }*/
    public void StartGame()
    {
        shootEnable = true;/*
        startTheTimer = true;
        Time.timeScale = 1;
        if (Application.isEditor)
        {
            Time.fixedDeltaTime = 0.01f;
        }*/
    }


    void Update()
    {/*
        if (startTheTimer)
        {
            startGameTimer += Time.deltaTime;
        }
        if (startGameTimer > startCountdownLength)
        {
            globalGameStart = true;
            shootEnable = true;
            startTheTimer = false;
            startGameTimer = 0;
        }*/
        if (Input.GetKeyDown(KeyCode.K))
        {

            if (!BasketballManager.instance.shouldLoadNextLevel)
            {
                BasketballPhysics basketball = BasketballManager.instance.GetBall();
                if (basketball != null)
                {
                    basketball.isActive = true;
                    basketball.GetComponent<Rigidbody2D>().simulated = true;
                    basketball.GetComponent<Rigidbody2D>().AddForce(new Vector3(5,5), ForceMode2D.Impulse);
                }
            }
        }


        if (shootEnable)
        {
            if (Input.touches.Length > 0)
            {
                var touch = Input.touches[0];
                Vector2 position = touch.position;
                float time = Time.time;

                if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                    return;

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        flickStartTime = time;
                        flickTime = 5;
                        timeIncrease();
                        couldBeSwipe = true;
                        GetVelocity = true;
                        touchStart = position;
                        break;
                    case TouchPhase.Moved:
                        if (Mathf.Abs(position.y - touchStart.y) < comfortZone)
                        {
                            couldBeSwipe = false;
                        }
                        else {
                            couldBeSwipe = true;
                        }
                        break;
                    case TouchPhase.Ended:
                        var swipeDist = (position - touchStart).magnitude;
                        if (couldBeSwipe || swipeDist > comfortZone)
                        {
                            flickEndTime = time;
                            GetVelocity = false;
                            touchEnd = position;
                            //var ball = Instantiate(ballPrefab, new Vector3(0, 2.6f, -11), Quaternion.identity) as GameObject;
                            GetSpeed();
                            GetAngle();
                            //ball.GetComponent<Rigidbody>().AddForce(new Vector3((worldAngle.x * ballSpeed), (worldAngle.y * ballSpeed), (worldAngle.z * ballSpeed)));
                            //PlayWhoosh();
                            if (!BasketballManager.instance.shouldLoadNextLevel)
                            {
                                BasketballPhysics basketball = BasketballManager.instance.GetBall();
                                if (basketball != null)
                                {
                                    basketball.isActive = true;
                                    basketball.GetComponent<Rigidbody2D>().simulated = true;
                                    //Vector3 force = new Vector3((worldAngle.x * ballSpeed), (worldAngle.y * ballSpeed), (worldAngle.z * ballSpeed)) * 10;
                                    float ftime = (flickEndTime - flickStartTime);
                                    Vector3 force = new Vector3((worldAngle.x / ftime), (worldAngle.y / ftime), (worldAngle.z / ftime));
                                    //Debug.Log("Mag: " + worldAngle.magnitude + "   Speed: " + ballSpeed + "  Time: " + (flickEndTime - flickStartTime) + "  force: " + force.magnitude );
                                    //Debug.Log(force);
                                    basketball.GetComponent<Rigidbody2D>().AddForce(force * System.Int32.Parse(forceText.text));
                                }
                            }
                        }
                        break;
                    default:
                        break;

                }//end switch case
                if (GetVelocity)
                {
                    flickTime++;
                }
            }
        }
        if (!shootEnable)
        {
            Debug.Log("shot disabled!");
        }
    }

    void timeIncrease()
    {
        if (GetVelocity)
        {
            flickTime++;
        }
    }

    void GetSpeed()
    {
        flickLength = 90;
        if (flickTime > 0)
        {
            ballVelocity = flickLength / (flickLength - flickTime);
            //ballVelocity = flickLength / (flickLength - 0.05f*(flickEndTime-flickStartTime));
            //ballVelocity = flickLength / ((flickEndTime - flickStartTime));
            //Debug.Log("Length: " + flickLength + "   Time: " + (flickEndTime - flickStartTime) + "   Velo: " + ballVelocity);
        }
        ballSpeed = ballVelocity * 30;
        ballSpeed = ballSpeed - (ballSpeed * 1.65f);
        if (ballSpeed <= -33)
        {
            ballSpeed = -33;
        }
        //Debug.Log("flick was " + flickTime);
        //Debug.Log("flick was " + (flickEndTime - flickStartTime));
        flickTime = 5;
    }

    void GetAngle()
    {
        //Debug.Log("touchend: " + touchEnd);
        //worldAngle = Camera.main.ScreenToWorldPoint(new Vector3(touchEnd.x, touchEnd.y + 800, ((Camera.main.nearClipPlane - 100) * 1.8f)));
        //worldAngle = Camera.main.ScreenToWorldPoint(new Vector3(0, 0,100));
        worldAngle = Camera.main.ScreenToWorldPoint(touchEnd) - Camera.main.ScreenToWorldPoint(touchStart);
    }

    public void AddToForce()
    {
        int num = System.Int32.Parse(forceText.text);
        num++;
        forceText.text = num.ToString();
    }

    public void SubtractForce()
    {
        int num = System.Int32.Parse(forceText.text);
        num--;
        forceText.text = num.ToString();
    }
}