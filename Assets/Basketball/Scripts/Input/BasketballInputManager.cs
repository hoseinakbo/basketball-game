﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketballInputManager : MonoBehaviour {

    public TouchPad touchpad;
    public float swipeLength = 4;
    Vector2 lastPos;
    float lastTime;
    [HideInInspector]
    public bool swiped;

    void Start () {

        touchpad.OnTouch += touchpad_OnTouch;
        swiped = false;
    }
    
    void touchpad_OnTouch(Vector2 position, float time, TouchEvent touchEvent)
    {
        switch (touchEvent)
        {
            case TouchEvent.Touched:
                lastPos = position;
                lastTime = time;
                break;
            case TouchEvent.Moved:
                if (!swiped)
                {
                    if (Mathf.Abs(Vector2.Distance(position, lastPos)) > swipeLength)
                    {
                        swiped = true;
                        Swiped(position, lastPos, Time.time-lastTime);
                    }
                }
                break;
            case TouchEvent.Detouched:
                swiped = false;
                break;
            default:
                break;
        }
    }
    public void Swiped(Vector2 pos1, Vector2 pos2, float time)
    {
        //Vector2 direction = (pos2 - pos1);
        //Debug.Log(direction / time);
        //phys.ThrowBall(direction / time);
        BasketballManager.instance.ThrowBasketball(pos1, pos2, time);
    }
}
