﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickManager : MonoBehaviour {

    public TouchPad touchpad;
    public Vector2 touchStart;
    public Vector2 touchEnd;
    public float flickTime = 5;
    public float flickLength = 0;
    public float ballVelocity;
    public float ballSpeed = 0;
    public Vector3 worldAngle;
    private bool GetVelocity = false;
    public float comfortZone;
    public bool couldbeswipe;
    public float startCountdownLength  = 0.0f;
    public bool startTheTimer = false;
    public static bool globalGameStart = false;
    public static bool shootEnable = false;
    private float startGameTimer = 0.0f;

    float flickStartTime;
    float flickEndTime;
 
    void Start()
    {
        touchpad.OnTouch += touchpad_OnTouch;
        startTheTimer = true;
        Time.timeScale = 1;
        if (Application.isEditor)
            Time.fixedDeltaTime = 0.01f;
    }

    void touchpad_OnTouch(Vector2 position, float time, TouchEvent touchEvent)
    {
        if (shootEnable)
        {
            switch (touchEvent)
            {
                case TouchEvent.Touched:
                    flickStartTime = time;
                    flickTime = 5;
                    timeIncrease();
                    couldbeswipe = true;
                    GetVelocity = true;
                    touchStart = position;
                    break;
                case TouchEvent.Moved:
                    if (Mathf.Abs(position.y - touchStart.y) < comfortZone)
                    {
                        couldbeswipe = false;
                    }
                    else {
                        couldbeswipe = true;
                    }
                    break;
                case TouchEvent.Detouched:
                    var swipeDist = (position - touchStart).magnitude;
                    if (couldbeswipe || swipeDist > comfortZone)
                    {
                        flickEndTime = time;
                        GetVelocity = false;
                        touchEnd = position;
                        //var ball = Instantiate(ballPrefab, new Vector3(0, 2.6f, -11), Quaternion.identity) as GameObject;
                        GetSpeed();
                        GetAngle();
                        //ball.GetComponent<Rigidbody>().AddForce(new Vector3((worldAngle.x * ballSpeed), (worldAngle.y * ballSpeed), (worldAngle.z * ballSpeed)));
                        //PlayWhoosh();
                        if (!BasketballManager.instance.shouldLoadNextLevel)
                        {
                            BasketballPhysics basketball = BasketballManager.instance.GetBall();
                            if (basketball != null)
                            {
                                basketball.isActive = true;
                                basketball.GetComponent<Rigidbody2D>().simulated = true;
                                //Vector3 force = new Vector3((worldAngle.x * ballSpeed), (worldAngle.y * ballSpeed), (worldAngle.z * ballSpeed)) * 10;
                                float ftime = (flickEndTime - flickStartTime);
                                Vector3 force = new Vector3((worldAngle.x / ftime), (worldAngle.y / ftime), (worldAngle.z / ftime));
                                //Debug.Log("Mag: " + worldAngle.magnitude + "   Speed: " + ballSpeed + "  Time: " + (flickEndTime - flickStartTime) + "  force: " + force.magnitude );
                                //Debug.Log(force);
                                basketball.GetComponent<Rigidbody2D>().AddForce(force*15);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
            if (GetVelocity)
            {
                flickTime++;
            }
        }
        if (!shootEnable)
        {
            Debug.Log("shot disabled!");
        }
    }
    void Update()
    {
        if (startTheTimer)
        {
            startGameTimer += Time.deltaTime;
        }
        if (startGameTimer > startCountdownLength)
        {
            globalGameStart = true;
            shootEnable = true;
            startTheTimer = false;
            startGameTimer = 0;
        }
    }

    void timeIncrease()
    {
        if (GetVelocity)
        {
            flickTime++;
        }
    }

    void GetSpeed()
    {
        flickLength = 90;
        if (flickTime > 0)
        {
            ballVelocity = flickLength / (flickLength - flickTime);
            //ballVelocity = flickLength / (flickLength - 0.05f*(flickEndTime-flickStartTime));
            //ballVelocity = flickLength / ((flickEndTime - flickStartTime));
            //Debug.Log("Length: " + flickLength + "   Time: " + (flickEndTime - flickStartTime) + "   Velo: " + ballVelocity);
        }
        ballSpeed = ballVelocity * 30;
        ballSpeed = ballSpeed - (ballSpeed * 1.65f);
        if (ballSpeed <= -33)
        {
            ballSpeed = -33;
        }
        //Debug.Log("flick was " + flickTime);
        //Debug.Log("flick was " + (flickEndTime - flickStartTime));
        flickTime = 5;
    }

    void GetAngle()
    {
        //Debug.Log("touchend: " + touchEnd);
        //worldAngle = Camera.main.ScreenToWorldPoint(new Vector3(touchEnd.x, touchEnd.y + 800, ((Camera.main.nearClipPlane - 100) * 1.8f)));
        //worldAngle = Camera.main.ScreenToWorldPoint(new Vector3(0, 0,100));
        worldAngle = Camera.main.ScreenToWorldPoint(touchEnd) - Camera.main.ScreenToWorldPoint(touchStart);
    }

}
