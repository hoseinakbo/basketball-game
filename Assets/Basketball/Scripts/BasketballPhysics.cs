﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketballPhysics : MonoBehaviour {

    Rigidbody2D myRigidbody;
    public bool isActive;

    bool trigger1;
    bool trigger2;

	// Use this for initialization
	void Start () {
        myRigidbody = GetComponent<Rigidbody2D>();
	}
	

    public void ThrowBall(Vector2 throwSpeed)
    {
        isActive = true;
        myRigidbody.simulated = true;
        myRigidbody.AddForce(throwSpeed/4, ForceMode2D.Impulse);
    }

    /* Currently not in use but works perfectly
    IEnumerator FadeBasketball(BasketballPhysics basketball)
    {
        yield return new WaitForSeconds(BasketballManager.instance.basketballDisapearTime);
        SpriteRenderer spriteRenderer = basketball.GetComponent<SpriteRenderer>();
        Color myColor = spriteRenderer.color;
        float alpha = 1;
        while (alpha > 0)
        {
            yield return new WaitForSeconds(Time.deltaTime);
            alpha -= 0.1f;
            myColor.a = alpha;
            spriteRenderer.color = myColor;
        }
        if (isActive)
        {
            BasketballManager.instance.DestroyBasketball(basketball);
        }
    }*/
    
    public void OnActivating()
    {
        trigger1 = false;
        trigger2 = false;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "BasketballNet")
        {
            //BasketballManager.instance.Scored();
            trigger1 = true;
            CheckScored();
        }
        else if (collider.tag == "BasketballNet2")
        {
            trigger2 = true;
            CheckScored();
        }
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "BasketballNet")
        {
            trigger1 = false;
        }
        else if (collider.tag == "BasketballNet2")
        {
            trigger2 = false;
        }
    }

    void CheckScored()
    {
        if(trigger1 && trigger2)
            BasketballManager.instance.Scored();
    }
}
