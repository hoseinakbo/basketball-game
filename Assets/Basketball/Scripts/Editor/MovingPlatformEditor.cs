﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MovingPlatform), true)]
public class MovingPlatformEditor : Editor
{

    void OnSceneGUI()
    {
        MovingPlatform element = (MovingPlatform)target;

        if (element.transform == null)
            return;

        if (element.isMoving)
        {
            Vector3 endPos = element.endPosition;
            endPos = Handles.FreeMoveHandle(endPos, Quaternion.identity, 0.3f, Vector3.one, Handles.DotCap);

            element.endPosition = endPos;
            if (!Application.isPlaying)
                element.startPosition = element.transform.position;

            Handles.DrawLine(element.startPosition, element.endPosition);
        }
    }
}
